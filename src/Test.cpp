#ifdef __cplusplus
#include <cstdlib>
#else
#include <stdlib.h>
#endif

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <iostream>

#include <sstream>
#include <vector>
using namespace std;

#include "../include/XmlService.h"
#include "../include/Texture.h"
#include "../include/Player.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;


bool generateAudio()
{

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 8192) < 0) {
		std::cout << "Error: " << Mix_GetError() << std::endl;
	}

	Mix_Music *bgs = Mix_LoadMUS("Sounds/Lost-Jungle_Looping.mp3");

	if (bgs == NULL) {
		std::cout << "Musik File not load!" << std::endl;
	}

	Mix_PlayMusic(bgs, -1);
	Mix_VolumeMusic(50);

	std::cout << "START MUSIK " << std::endl;



	return true;
}

int main(int argc, char** argv) {
	int iReturn = 0;

	// iReturn = personeMoving();

	char xmlPath[] = "Map/map_alistair.xml";
	XmlService* oXmlService = new XmlService(xmlPath);
	auto Map = oXmlService->generateLayersVector();

	SDL_Window* window = NULL;
	SDL_Texture *currentTailset = NULL;
	SDL_Renderer *renderTarget = NULL;

	SDL_Init(SDL_INIT_VIDEO);
	int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
	if (IMG_Init(imgFlags) != imgFlags) {
		std::cout << "Error Image not Init" << IMG_GetError() << std::endl;
	}

	window = SDL_CreateWindow("First Game", SDL_WINDOWPOS_CENTERED,
			SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT,
			SDL_WINDOW_SHOWN);

	generateAudio();

	renderTarget = SDL_CreateRenderer(window, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	currentTailset = Texture::LoadTexture("Images/tileset-alistair.png", renderTarget);

	int tailSize = 16;
	bool bIsRunning = true;
	SDL_Event ev;

	Player* oPlayer = new Player(renderTarget);
	oPlayer->init("Images/char_sprites.png", 32, 32);

	while (true == bIsRunning) {

		SDL_PollEvent(&ev);
		if (ev.type == SDL_QUIT) {
			std::cout << "You Push Quit" << std::endl;
			bIsRunning = false;
		}

		oPlayer->processEvent(ev);

		SDL_RenderClear(renderTarget);

		for (int unsigned i = 0; i < Map.size(); i++) {
			std::vector<std::vector<std::string> > mVector = Map.at(i);
			for (int unsigned line = 0; line < mVector.size(); line++) {
				std::vector<std::string> mVec = mVector.at(line);

				for (int unsigned column = 0; column < mVec.size(); column++) {
					std::string tailCoordinate = mVec.at(column);

					auto aTailCoordinate = oXmlService->explode(tailCoordinate,
							'.');

					SDL_Rect tailRec;
					SDL_Rect tailPosition;

					if (2 == aTailCoordinate.size()) {

						tailRec.x = stoi(aTailCoordinate.at(0)) * tailSize;
						tailRec.y = stoi(aTailCoordinate.at(1)) * tailSize;
						tailRec.h = tailRec.w = tailSize;
						tailPosition.h = tailPosition.w = tailSize;
						tailPosition.x = column * 16;
						tailPosition.y = line * 16;
						SDL_RenderCopy(renderTarget, currentTailset, &tailRec, &tailPosition);
					}
				}
			}
		}

		oPlayer->checkCollision(Map);
		oPlayer->render();

		SDL_RenderPresent(renderTarget);
	}

	return iReturn;
}
