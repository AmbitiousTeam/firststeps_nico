/*
 * Texture.cpp
 *
 *  Created on: 10.08.2016
 *      Author: nico
 */

#include "../include/Texture.h"

Texture::Texture() {
	// TODO Auto-generated constructor stub

}

Texture::~Texture() {
	// TODO Auto-generated destructor stub
}

SDL_Texture* Texture::LoadTexture(std::string filePath, SDL_Renderer *renderTarget)
{
	SDL_Texture *texture = NULL;
	SDL_Surface *surface = IMG_Load(filePath.c_str());
	if (surface == NULL) {
		std::cout << "Error Load Image" << std::endl;
	} else {
		texture = SDL_CreateTextureFromSurface(renderTarget, surface);
		if (texture == NULL) {
			std::cout << "Error Load Image" << std::endl;
		}
	}

	SDL_FreeSurface(surface);

	return texture;
}

