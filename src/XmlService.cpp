#include "../include/XmlService.h"
#include <iostream>
#include <sstream>
#include <vector>
#include "rapidxml/rapidxml_utils.hpp"

//using namespace rapidxml;

XmlService::XmlService(char *sXmlPath)
{
    rapidxml::file<> xmlFile(sXmlPath);
    rapidxml::xml_document<> doc;
    doc.parse<0>(xmlFile.data());

    this->pRoot = doc.first_node();
}


// XmlService::getNodeByName(char *);

std::vector<std::string> XmlService::explode(const std::string& str, char delimiter)
{
    std::vector<std::string> tokens;
    std::stringstream tokenStream(str);
    std::string tempStr;

    while(std::getline(tokenStream, tempStr, delimiter))
        tokens.push_back(tempStr);

    return tokens;
}

std::vector<std::vector <std::vector <std::string> > > XmlService::generateLayersVector()
{

    std::vector<std::string> mLayers;
    std::vector<std::vector <std::vector <std::string> > > mPoints;


    rapidxml::xml_node<> *pNode = this->pRoot->first_node("layers");
    rapidxml::xml_attribute<> *pAttr = pNode->first_attribute("name");
    std::string strValue = pAttr->value();

    for (rapidxml::xml_node<> *pLayer = pNode->first_node("layer"); pLayer; pLayer = pLayer->next_sibling()) {

        mLayers.push_back(pLayer->value());
    }

    for (int unsigned i = 0; i < mLayers.size();i++) {

        //char delimiter[]
        // std::vector<std::string> mVectors = ;
        std::vector<std::vector <std::string> > mPoint;

        std::vector<std::string> mVector = this->explode(mLayers[i], '\r\n');
        for (int unsigned i = 0;i < mVector.size();i++) {

            std::string strLine = mVector.at(i);
            if (strLine.substr(0,1) == ",") {
            	strLine = mVector.at(i).erase(0,1);
            } else {
            	strLine = mVector.at(i);
            }

            mPoint.push_back(this->explode(strLine, ','));
        }

        mPoints.push_back(mPoint);
    }

    return mPoints;
/*
    for (int unsigned i = 0; i < mPoints.size();i++) {
        std::vector<std::vector <std::string> > mVector = mPoints.at(i);
        for (int unsigned a = 0; a < mVector.size(); a++) {
        std::vector<std::string> mVec = mVector.at(a);

            std::cout << "Reihe" << std::endl;
            std::cout << a << std::endl;

            for (int unsigned x = 0; x < mVec.size(); x++) {
                std::cout << mVec.at(x) << std::endl;

            }
            std::cout << "\r\n" << std::endl;
        }

    }
    */

}

XmlService::~XmlService()
{
    //dtor
}
