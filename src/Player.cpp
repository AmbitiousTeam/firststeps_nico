/*
 * Player.cpp
 *
 *  Created on: 10.08.2016
 *      Author: nico
 */

#include "../include/Player.h"
#include "../include/Texture.h"
#include <vector>

Player::Player(SDL_Renderer* Renderer) :
		playerPosition(NULL) {

	this->_Renderer = Renderer;
	this->playerPicturePoints = new SDL_Rect();
	this->playerPosition = new SDL_Rect();

	// TODO Auto-generated constructor stub

}

bool Player::init(std::string graphicPath, int spriteHight, int spriteWitdh) {
	this->_Graphic = Texture::LoadTexture(graphicPath, this->_Renderer);
	this->iSprintHight = spriteHight;
	this->iSprintWitdh = spriteWitdh;

	this->playerPicturePoints->h = spriteHight;
	this->playerPicturePoints->w = spriteWitdh;
	this->playerPosition->h = spriteHight;
	this->playerPosition->w = spriteWitdh;

	this->_StepSound = Mix_LoadWAV("Sounds/FOOTSTEPS.wav");

	return true;
}

SDL_Renderer* Player::getRenderer() {
	return this->_Renderer;
}


bool Player::checkCollision(std::vector <std::vector <std::vector <std::string > > > oMap)
{
	int iMapTileSize = 16;
	int checkx, checky, checkxRight = 0;
	float iMapXPoint, iMapYPoint, iMapxPointRight = 0;

	if (this->_iMoveX > 0) {
		 checkx = this->playerPosition->x + this->playerPosition->w + 1;
	} else if (this->_iMoveX < 0) {
		 checkx = this->playerPosition->x - 1;
	} else {
		checkx = this->playerPosition->x;
	}

	if (this->_iMoveY > 0) {
		checky = this->playerPosition->y + this->playerPosition->h + 1;
	} else if (this->_iMoveY <0 ) {
		checky = this->playerPosition->y - 1 + floor((float)this->playerPosition->h / 1.3);
	} else {
		checky = this->playerPosition->y + this->playerPosition->h;
	}

	if (this->_iMoveY != 0) {
		checkxRight = checkx + this->playerPicturePoints->w;
	}


	if (0 != this->_iMoveX || 0 != this->_iMoveY) {
		iMapXPoint = (float)checkx / (float)iMapTileSize;
		iMapYPoint = (float)checky / (float)iMapTileSize;
		iMapxPointRight = (float)checkxRight / (float)iMapTileSize;

		std::cout << " CHECK X POINT: " << checkx << " CHECK Y POINT: " << checky << std::endl;
		std::cout << "X POINT: " << iMapXPoint << " Y POINT: " << iMapYPoint << std::endl;

		if (0 > iMapXPoint || 0 > iMapYPoint) {
			this->_iMoveY = 0;
			this->_iMoveX = 0;
			return true;
		}

		for (int unsigned i = 0;i < oMap.size(); i++) {

			std::string sText = oMap.at(i).at(floor(iMapYPoint)).at(floor(iMapXPoint));

			if ("0.2" != sText && "-1" != sText) {
				this->_iMoveX = 0;
				std::cout << "X STOP STOP \n" << std::endl;
			}
			std::cout << sText << "\n" << std::endl;

			for (int unsigned x = floor(iMapXPoint); x <= floor(iMapxPointRight); x++ ) {
				std::cout << " CHECK X POINT: " << x << " CHECK Y POINT: " << iMapYPoint << std::endl;
				std::string sText = oMap.at(i).at(iMapYPoint).at(floor(x));
				if ("0.2" != sText && "-1" != sText) {
					this->_iMoveY = 0;
					std::cout << "Y STOP STOP \n" << std::endl;
				}

			}
		}
	}

	return true;
}



bool Player::render() {

	this->_iAnimationFrequenz++;
	if (this->_iMoveX != 0 || this->_iMoveY != 0) {
		Mix_PlayChannel(-1, this->_StepSound, 0);
		playerPosition->x += this->_iMoveX;
		playerPosition->y += this->_iMoveY;

		std::cout << "AnimationFrequenz: " << this->_iAnimationFrequenz << std::endl;

		if (5 < this->_iAnimationFrequenz) {
			this->playerPicturePoints->x += this->iSprintWitdh;
			if (this->playerPicturePoints->x >= this->iSprintWitdh * 3) {
				this->playerPicturePoints->x = 0;
			}
			this->_iAnimationFrequenz = 0;
		}
	} else {
		Mix_HaltChannel(-1);
		playerPicturePoints->x = this->iSprintWitdh;
	}


	SDL_RenderCopy(this->_Renderer, this->_Graphic, this->playerPicturePoints,
			this->playerPosition);

	return true;

}

bool Player::processEvent(SDL_Event oSdlEvent) {

	if (oSdlEvent.type == SDL_KEYDOWN) {
		switch (oSdlEvent.key.keysym.sym) {
		case SDLK_LEFT:
			playerPicturePoints->y = this->iSprintHight;
			this->_iMoveX = -1;
			this->_iMoveY = 0;
			break;
		case SDLK_RIGHT:
			playerPicturePoints->y = this->iSprintHight * 2;
			this->_iMoveX = 1;
			this->_iMoveY = 0;
			break;
		case SDLK_UP:
			playerPicturePoints->y = this->iSprintHight * 3;
			this->_iMoveY = -1;
			this->_iMoveX = 0;
			break;
		case SDLK_DOWN:
			playerPicturePoints->y = 0;
			this->_iMoveY = +1;
			this->_iMoveX = 0;
			break;

		case SDLK_r:
			playerPosition->x = 0;
			playerPosition->y = this->iSprintHight;
			break;
		}
	}

	if (oSdlEvent.type == SDL_KEYUP) {
		switch (oSdlEvent.key.keysym.sym) {
				case SDLK_LEFT:
				case SDLK_RIGHT:
					this->_iMoveX = 0;
					break;
				case SDLK_UP:
				case SDLK_DOWN:
					this->_iMoveY = 0;
					break;
		}
	}

	/*
	 else if (SDL_KEYUP == oSdlEvent.type) {
	 switch (oSdlEvent.key.keysym.sym) {
	 case SDLK_LEFT:
	 case SDLK_RIGHT:
	 case SDLK_UP:
	 case SDLK_DOWN:
	 spliteLengthX = 0;
	 playerRect.x = 0;
	 moveY = 0;
	 moveX = 0;
	 break;
	 }
	 }
	 */

	return true;
}

Player::~Player() {
	// TODO Auto-generated destructor stub
}

