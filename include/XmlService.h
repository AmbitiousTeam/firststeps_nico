#ifndef XMLSERVICE_H
#define XMLSERVICE_H

#include <iostream>
#include "../src/rapidxml/rapidxml_utils.hpp"

class XmlService
{
    public:
        XmlService(char *sXmlPath);
        virtual ~XmlService();

        std::vector<std::vector <std::vector <std::string> > > generateLayersVector();
        std::vector<std::string> explode(std::string const & s, char delim);
    protected:
    private:
        rapidxml::xml_node<> *pRoot;

};

#endif // XMLSERVICE_H
