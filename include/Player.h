/*
 * Player.h
 *
 *  Created on: 10.08.2016
 *      Author: nico
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <iostream>
#include <vector>

class Player {
public:

	virtual ~Player();

	Player(SDL_Renderer* Renderer);
	SDL_Renderer* getRenderer();
	bool init(std::string graphicPath, int spriteHight, int spriteWitdh);
	bool processEvent(SDL_Event oSdlEvent);
	bool checkCollision(std::vector <std::vector <std::vector <std::string > > > oMap);
	bool render();

private:
	SDL_Rect* playerPosition;
	SDL_Rect* playerPicturePoints;

	SDL_Renderer *_Renderer;
	SDL_Texture *_Graphic;

	int iSprintHight;
	int iSprintWitdh;
	int _iMoveX = 0;
	int _iMoveY = 0;
	int _iAnimationFrequenz = 0;

	Mix_Chunk *_StepSound;
};

#endif /* PLAYER_H_ */
