/*
 * Texture.h
 *
 *  Created on: 10.08.2016
 *      Author: nico
 */

#ifndef TEXTURE_H_
#define TEXTURE_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <cstdlib>

class Texture {
public:
	Texture();
	virtual ~Texture();

	static SDL_Texture* LoadTexture(std::string filePath, SDL_Renderer *renderTarget);

};

#endif /* TEXTURE_H_ */
